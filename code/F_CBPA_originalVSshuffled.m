% CBPA of MSE output (Let's start with vanilla)

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/fieldtrip-20170904'); ft_defaults;

pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/A2_MSE_phaseShuffled/';
mseShuffled = load([pn.root, 'B_data/E_mseMerged.mat'], 'mseMerged', 'IDs');
    
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
mseOriginal = load([pn.root, 'B_data/E_mseMerged.mat'], 'mseMerged', 'IDs');
    
methodLabels = {'MSEVanilla', 'MSEfiltskip', ...
    'Rvanilla', 'Rfiltskip'};

% create ratio scores (note that cells vary due to only EO condition being estimated for shuffled data)

mseMerged{1,1}.MSEVanilla = mseOriginal.mseMerged{1,2}.MSEVanilla./mseShuffled.mseMerged{1,1}.MSEVanilla;
mseMerged{1,1}.MSEfiltskip = mseOriginal.mseMerged{1,2}.MSEfiltskip./mseShuffled.mseMerged{1,1}.MSEfiltskip;
mseMerged{1,1}.Rvanilla = mseOriginal.mseMerged{1,2}.Rvanilla./mseShuffled.mseMerged{1,1}.Rvanilla;
mseMerged{1,1}.Rfiltskip = mseOriginal.mseMerged{1,2}.Rfiltskip./mseShuffled.mseMerged{1,1}.Rfiltskip;

mseMerged{2,1}.MSEVanilla = mseOriginal.mseMerged{2,2}.MSEVanilla./mseShuffled.mseMerged{2,1}.MSEVanilla;
mseMerged{2,1}.MSEfiltskip = mseOriginal.mseMerged{2,2}.MSEfiltskip./mseShuffled.mseMerged{2,1}.MSEfiltskip;
mseMerged{2,1}.Rvanilla = mseOriginal.mseMerged{2,2}.Rvanilla./mseShuffled.mseMerged{2,1}.Rvanilla;
mseMerged{2,1}.Rfiltskip = mseOriginal.mseMerged{2,2}.Rfiltskip./mseShuffled.mseMerged{2,1}.Rfiltskip;

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = mseMerged{1,1}.label;

%% CBPA: between-group: EO: OA vs. YA

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_indepsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 3;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, mseShuffled.mseMerged{1,1});

N_1 = size(mseShuffled.mseMerged{1,1}.MSEVanilla,1);
N_2 = size(mseShuffled.mseMerged{2,1}.MSEVanilla,1);
cfgStat.design = zeros(1,N_1+N_2);
cfgStat.design(1,1:N_1) = 1;
cfgStat.design(1,N_1+1:end) = 2;
    
for indMethod = 1:numel(methodLabels)
    cfgStat.parameter = methodLabels{indMethod};
    [stat{1,indMethod}] = ft_freqstatistics(cfgStat, mseMerged{2,1}, mseMerged{1,1});
end

save(['/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/A2_MSE_phaseShuffled/B_data/F_CBPA_originalVSshuffled.mat'], 'stat', 'methodLabels')