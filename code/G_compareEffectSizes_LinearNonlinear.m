% Statistically compare effect sizes between different methods.

%% load MSE topography with different filter settings

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
Original = load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat']);
pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/A2_MSE_phaseShuffled/';
Shuffled = load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat']);
Ratio = load([pn.root, 'B_data/F_CBPA_originalVSshuffled.mat'], 'stat', 'methodLabels');

% Note that time scales are inverted between statistical output matrix and
% original data matrices!

% extract individual values from statistical cluster (Vanilla version)

FineMSEVanillaCluster = find(Original.stat{4,1}.posclusterslabelmat==1);
CoarseMSEVanillaCluster = find(Original.stat{4,1}.negclusterslabelmat==1);

load([pn.root, 'B_data/E_mseMerged.mat'])

%% create t-value ratios

Original_fine = mean(mean(Original.stat{4,1}.stat(FineMSEVanillaCluster)));
Original_coarse = mean(mean(Original.stat{4,1}.stat(CoarseMSEVanillaCluster)));

Shuffled_fine = mean(mean(Shuffled.stat{1,1}.stat(FineMSEVanillaCluster)));
Shuffled_coarse = mean(mean(Shuffled.stat{1,1}.stat(CoarseMSEVanillaCluster)));

Ratio_fine = mean(mean(Ratio.stat{1,1}.stat(FineMSEVanillaCluster)));
Ratio_coarse = mean(mean(Ratio.stat{1,1}.stat(CoarseMSEVanillaCluster)));

100*Shuffled_fine/Original_fine
100*Shuffled_coarse/Original_coarse

100*Ratio_fine/Original_fine
100*Ratio_coarse/Original_coarse

