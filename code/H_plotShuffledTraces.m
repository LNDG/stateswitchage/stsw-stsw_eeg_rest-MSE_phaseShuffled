
SignalOriginal = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/B_MSE_Segmented_Dim_Input/1132_EO_MSE_IN.mat');
SignalShuffled = load('/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/A2_MSE_phaseShuffled/B_data/B_MSE_Segmented_Dim_Input/1132_EO_MSE_IN.mat');

h = figure; 
%cla; hold on; plot(zscore(SignalOriginal.data.trial{6}(59,:)), 'k'); plot(zscore(SignalShuffled.data.trial{6}(59,:))+5, 'r');
cla; hold on; plot(zscore(SignalOriginal.data.trial{31}(59,:))+8, 'k', 'LineWidth', 2); plot(zscore(SignalShuffled.data.trial{31}(59,:)), 'r', 'LineWidth', 2);
%cla; hold on; plot(zscore(SignalOriginal.data.trial{31}(12,:)), 'k'); plot(zscore(SignalShuffled.data.trial{31}(12,:))+5, 'r');
legend({'Original signal', 'Surrogate signal'}, 'location', 'East'); legend('boxoff')
axis off
set(findall(gcf,'-property','FontSize'),'FontSize',20)

pn.plotFolder = '/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/A2_MSE_phaseShuffled/C_figures/';
figureName = 'Z_shuffledTraces';
saveas(h, [pn.plotFolder, figureName], 'eps');
saveas(h, [pn.plotFolder, figureName], 'png');
